#pragma once
#include <string>
#include "UtilityEnum.h"
#include "Utility.h"
#include <vector>

class Player {
private:
	std::string username;
	PlayerType playerType;
	CellStat ShipField[10][10];
	CellStat ShootField[10][10];
	std::vector<int> ShipList = {4, 3, 2, 1};
public:
	Player() {
		username.clear();
		playerType = PlayerType::EMPTYSLOT;
		for (auto i = 0; i < 10; i++)
			for (auto j = 0; j < 10; j++) {
				ShipField[i][j] = CellStat::WATER;
				ShootField[i][j] = CellStat::WATER;
			}
	}
	Player(std::string name) {
		username = name;
		playerType = PlayerType::USER;
		for (auto i = 0; i < 10; i++)
			for (auto j = 0; j < 10; j++) {
				ShipField[i][j] = CellStat::WATER;
				ShootField[i][j] = CellStat::WATER;
			}
	}
	Player(bool isAI) {
		username = "Jack The Sparrow";
		playerType = PlayerType::AI;
		for (auto i = 0; i < 10; i++)
			for (auto j = 0; j < 10; j++) {
				ShipField[i][j] = CellStat::WATER;
				ShootField[i][j] = CellStat::WATER;
			}
	}

	std::string get_username() {
		return username;
	}
	void set_username(std::string name) {
		username = name;
	}

	PlayerType get_playerType() {
		return playerType;
	}
	void set_playerType(PlayerType type) {
		playerType = type;
	}

	void place_ship(std::string c1, std::string c2) {
		if (isShip(c1, c2, std::ref(ShipList))) {

		}
	}
};