#pragma once
enum class CellStat {
	WATER,
	SHIP,
	MISS,
	SSHIP
};

enum class PlayerOrder {
	FIRST,
	SECOND
};

enum class PlayerType {
	USER,
	AI,
	EMPTYSLOT
};